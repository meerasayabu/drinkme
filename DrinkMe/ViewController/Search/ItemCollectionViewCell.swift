//
//  ItemCollectionViewCell.swift
//  DrinkMe
//
//  Created by Meera on 07/04/22.
//

import UIKit
import Kingfisher

protocol ItemCollectionViewCellDelegate: NSObjectProtocol {
    func didTapViewButton(item: CocktailDrink?)
}

class ItemCollectionViewCell: UICollectionViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var itemSubTitle: UILabel!
    
    //MARK: Variables
    weak var delegate: ItemCollectionViewCellDelegate?
    private var item: CocktailDrink?
    
    //MARK: Action
    @IBAction func view(_ sender: UIButton) {
        delegate?.didTapViewButton(item: item)
    }
    
    func configureCell(drink: CocktailDrink) {
        
        item = drink
        
        itemTitle.text = drink.strDrink
        itemSubTitle.text = drink.strCategory
        itemImage.layer.cornerRadius = 10
        
        if let idDrink = drink.idDrink, let doublePrice = Double(idDrink) {
            price.text = "$" + "\(doublePrice.round(to: 3))"
        }
        
        viewButton.clipsToBounds = true
        viewButton.layer.cornerRadius = 4
        viewButton.backgroundColor = .clear
        viewButton.layer.borderWidth = 1
        viewButton.layer.borderColor = UIColor.orange.cgColor
        
        if let urlString = drink.strDrinkThumb, let url = URL(string: urlString) {
            itemImage.kf.setImage(with: url)
        }
    }
}
