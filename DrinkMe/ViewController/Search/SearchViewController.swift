//
//  ViewController.swift
//  DrinkMe
//
//  Created by Meera on 06/04/22.
//

import UIKit

protocol SearchViewControllerProtocol: AnyObject {
    var viewModel: SearchViewModelProtocol! { get set }
    func displaySearchResults()
}

class SearchViewController: UIViewController, SearchViewControllerProtocol, Coordinating, Storyboardable {
    
    //Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var seachCollectionView: UICollectionView!
    @IBOutlet weak var noResultLabel: UILabel!
    
    // Variables
    var viewModel: SearchViewModelProtocol!
    private var cellsize = CGSize.zero
    var coordinator: Coordinator?
    
    //MARK: Constant
    private let initialQuary = "Rum"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        doSearch(quary: initialQuary)
    }
    
    private func setup() {
        seachCollectionView.delegate = self
        seachCollectionView.dataSource = self
        searchBar.delegate = self
        
        seachCollectionView.showsVerticalScrollIndicator = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        if let flowLayout = seachCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let viewWidth = UIScreen.main.bounds.size.width
            let cellWidth = (viewWidth - 45) / 2
            cellsize = CGSize(width: cellWidth, height: cellWidth + (cellWidth - 20) / 2)
            flowLayout.itemSize = cellsize
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func doSearch(quary: String?) {
        viewModel.searchDrink(quary: quary)
    }
    
    internal func displaySearchResults() {
        guard let result = viewModel.searchResults, !result.isEmpty else {
            noResultLabel.isHidden = false
            seachCollectionView.isHidden = true
            return
        }
        noResultLabel.isHidden = true
        seachCollectionView.isHidden = false
        seachCollectionView.reloadData()
    }
    
    private func presentDetailsScreen(drink: CocktailDrink?) {
        coordinator?.navigation(event: .navigateToDetailsScreen, drinkItem: drink)
    }
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.searchResults?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! ItemCollectionViewCell
        cell.delegate = self
        cell.configureCell(drink: viewModel.searchResults![indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = viewModel.searchResults?[indexPath.row]
        presentDetailsScreen(drink: item)
    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellsize
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        doSearch(quary: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}

extension SearchViewController: ItemCollectionViewCellDelegate {
    func didTapViewButton(item: CocktailDrink?) {
        view.endEditing(true)
        presentDetailsScreen(drink: item)
    }
}

