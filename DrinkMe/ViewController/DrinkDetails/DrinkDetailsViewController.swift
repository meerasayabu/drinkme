//
//  DrinkDetailsViewController.swift
//  DrinkMe
//
//  Created by Meera on 07/04/22.
//

import UIKit
import Kingfisher

protocol DrinkDetailsViewControllerProtocol: AnyObject {
    var viewModel: DrinkDetailViewModelProtocol! { get set }
}

class DrinkDetailsViewController: UIViewController, Storyboardable, Coordinating, DrinkDetailsViewControllerProtocol {
    
    //MARK: Outlets
    @IBOutlet weak var drinkIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var instructionLabel: UIPaddingLabel!
    @IBOutlet weak var measureStackView: UIStackView!
    @IBOutlet weak var ingredientsStackView: UIStackView!
    
    //MARK: Variables
    var coordinator: Coordinator?
    var viewModel: DrinkDetailViewModelProtocol!
    
    //MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        if let urlString = viewModel.drinkItem?.strDrinkThumb, let url = URL(string: urlString) {
            drinkIcon.kf.setImage(with: url)
            drinkIcon.layer.cornerRadius = 5
            drinkIcon.clipsToBounds = true
        }
        
        titleLabel.text = viewModel.drinkItem?.strDrink
        
        if let idDrink = viewModel.drinkItem?.idDrink, let doublePrice = Double(idDrink) {
            priceLabel.text = "$" + "\(doublePrice.round(to: 3))"
        }
        
        configureCategoryLabel()
        configureIngredientStackView()
        configureMeasureStackView()
        
        instructionLabel.text = viewModel.drinkItem?.strInstructions
        instructionLabel.layer.cornerRadius = 5
        instructionLabel.clipsToBounds = true
    }
    
    private func configureCategoryLabel() {
        var tempCategory: String = ""
        
        if let categoryList = viewModel.drinkItem?.categoryList {
            for (index, element) in categoryList.enumerated() {
                tempCategory += element!
                if index < categoryList.count - 1, element != nil {
                    tempCategory += " | "
                }
            }
        }
        
        categoryLabel.text = tempCategory
    }
    
    private func configureIngredientStackView() {
        viewModel.drinkItem?.ingredientList.forEach({
            guard let ingredient = $0, !ingredient.isEmpty else {
                return
            }
            ingredientsStackView.addArrangedSubview(label(text: ingredient))
        })
    }
    
    private func configureMeasureStackView()  {
        viewModel.drinkItem?.measureList.forEach({
            guard let measure = $0, !measure.isEmpty else {
                return
            }
            measureStackView.addArrangedSubview(label(text: measure))
        })
    }
    
    private func label(text: String) -> UIPaddingLabel {
        let label = UIPaddingLabel()
        label.text = text
        label.font = label.font.withSize(15)
        label.backgroundColor = UIColor(red: 152, green: 152, blue: 152, alpha: 0.3)
        label.layer.cornerRadius = 3
        label.clipsToBounds = true
        return label
    }
    
}
