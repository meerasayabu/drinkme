//
//  AppCoordinator.swift
//  DrinkMe
//
//  Created by Meera on 07/04/22.
//

import Foundation
import UIKit

enum Event {
    case navigateToDetailsScreen
}

protocol Coordinator {
    var navigationController: UINavigationController? { get set }
    func navigation(event: Event, drinkItem: CocktailDrink?)
    func start()
}

protocol Coordinating {
    var coordinator: Coordinator? { get set }
}
