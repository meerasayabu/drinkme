//
//  MainCoordinator.swift
//  DrinkMe
//
//  Created by Meera on 07/04/22.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    var navigationController: UINavigationController?
    
    func navigation(event: Event, drinkItem: CocktailDrink?) {
        switch event {
        case .navigateToDetailsScreen:
            
            let drinkDetailsViewController = DrinkDetailsViewController.instantiate()
            
            let viewModel = DrinkDetailsViewModel()
            viewModel.viewController = drinkDetailsViewController
            
            viewModel.drinkItem = drinkItem
            drinkDetailsViewController.viewModel = viewModel
            drinkDetailsViewController.coordinator = self
            
            navigationController?.present(drinkDetailsViewController, animated: true, completion: nil)
        }
    }
    
    func start() {
        
        let viewModel = SearchViewModel()
        viewModel.apiClient = SearchAPIClient(apiclient: APIClient(authorizationManager: NetworkManager.shared))
        
        let searchViewController = SearchViewController.instantiate()
        searchViewController.viewModel = viewModel
        viewModel.viewController = searchViewController
        searchViewController.coordinator = self
        
        navigationController?.setViewControllers([searchViewController], animated: false)
    }
}
