//
//  SearchResultDrinkList.swift
//  DrinkMe
//
//  Created by Meera on 06/04/22.
//

import Foundation

struct CocktailDrinkSearchResult: Codable {
    let drinks: [CocktailDrink]?
}

struct CocktailDrink: Codable {
    let idDrink: String?
    let strDrink: String?
    let strCategory: String?
    let strIBA: String?
    let strAlcoholic: String?
    let strGlass: String?
    
    let strInstructions: String?
    let strDrinkThumb: String?
    
    let strIngredient1: String?
    let strIngredient2: String?
    let strIngredient3: String?
    let strIngredient4: String?
    let strIngredient5: String?
    
    let strMeasure1: String?
    let strMeasure2: String?
    let strMeasure3: String?
    let strMeasure4: String?
    
    let strImageSource: String?
    let strImageAttribution: String?
}

extension CocktailDrink {
    var ingredientList: [String?] {
        return [strIngredient1, strIngredient2, strIngredient3, strIngredient4, strIngredient5]
    }
    
    var measureList: [String?] {
        return [strMeasure1, strMeasure2, strMeasure3, strMeasure4]
    }
    
    var categoryList: [String?] {
        return [strCategory, strAlcoholic, strGlass]
    }
}
