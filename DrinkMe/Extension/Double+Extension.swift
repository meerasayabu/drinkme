//
//  Double+Extension.swift
//  DrinkMe
//
//  Created by Meera on 07/04/22.
//

import Foundation

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
