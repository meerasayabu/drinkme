//
//  SearchViewModel.swift
//  DrinkMe
//
//  Created by Meera on 06/04/22.
//

import Foundation

protocol SearchViewModelProtocol {
    var viewController: SearchViewController? {get set}
    var apiClient: SearchAPIClientProtocol! { get set}
    var searchResults: [CocktailDrink]? { get set}
    func searchDrink(quary: String?)
}

class SearchViewModel: SearchViewModelProtocol {
    
    //MARK: Variables
    var apiClient: SearchAPIClientProtocol!
    weak var viewController: SearchViewController?
    var searchResults: [CocktailDrink]?
    
    //MARK: Methods
    func searchDrink(quary: String?) {
        apiClient.fetchSearchResults(quaryString: quary) { [weak self] cocktailDrinkSearchResult, error in
            DispatchQueue.main.async {
                self?.searchResults = cocktailDrinkSearchResult?.drinks
                self?.viewController?.displaySearchResults()
            }
        }
    }
    
}
