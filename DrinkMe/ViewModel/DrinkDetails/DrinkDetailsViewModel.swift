//
//  DrinkDetailsViewModel.swift
//  DrinkMe
//
//  Created by Meera on 07/04/22.
//

import Foundation


protocol DrinkDetailViewModelProtocol {
    var viewController: DrinkDetailsViewController? {get set}
    var drinkItem: CocktailDrink? { get set }
}

class DrinkDetailsViewModel: DrinkDetailViewModelProtocol {
    
    //MARK: Variables
    weak var viewController: DrinkDetailsViewController?
    var drinkItem: CocktailDrink?
    
    //MARK: Methods
}
