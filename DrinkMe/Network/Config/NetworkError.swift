//
//  NetworkError.swift
//  DrinkMe
//
//  Created by Meera on 06/04/22.
//

import Foundation


public enum NetworkError: Error {
    case badRequest
    case failed
    case noResponseData
    case unableToDecodeResponseData(errorDescription: String)
    case other(message: String?)
    
    var localizedDescription: String{
        var message: String = ""
        
        switch self{
        case .badRequest:
            message = "Bad request"
        case .failed:
            message = "API request failed"
        case .noResponseData:
            message = "Empty response data"
        case .unableToDecodeResponseData:
            message = "Unable to decode response object"
        case let .other(errorMessage):
            message = errorMessage ?? ""
        }
        return message
    }
}
