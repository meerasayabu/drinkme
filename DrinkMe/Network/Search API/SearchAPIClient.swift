//
//  SearchAPIClient.swift
//  DrinkMe
//
//  Created by Meera on 06/04/22.
//

import Foundation

let hostURL = "https://thecocktaildb.com/"

protocol SearchAPIClientProtocol {
    var apiClient: APIClient! { get set }
    
    init(apiclient: APIClient)
    func fetchSearchResults(quaryString:String? ,completion: @escaping (CocktailDrinkSearchResult?, Error?) -> Void)
}

struct SearchAPIClient: SearchAPIClientProtocol {

    var apiClient: APIClient!
    
    init(apiclient: APIClient) {
        self.apiClient = apiclient
    }
    
    func fetchSearchResults(quaryString:String? ,completion: @escaping (CocktailDrinkSearchResult?, Error?) -> Void) {
        
        let seachAPIData = SearchAPIData(
            parameters: RequestParams(
                urlParameters: ["s": quaryString ?? ""], bodyParameters: nil)
        )
        
        apiClient.fetch(request: seachAPIData, basePath: hostURL) { (data, response) in
            let decoder = JSONDecoder()
            let searchResultList = try? decoder.decode(CocktailDrinkSearchResult.self, from: data)
            completion(searchResultList, nil)
        } failure: { (data, response, error) in
            completion(nil, error)
        }
    }
    
}
