//
//  SearchAPIData.swift
//  DrinkMe
//
//  Created by Meera on 07/04/22.
//

import Foundation

struct SearchAPIData: APIData {
    
    //URL: https://thecocktaildb.com/api/json/v1/1/search.php?s=mojito
    
    var path: String { "api/json/v1/1/search.php" }
    var method: HTTPMethod { .get }
    var parameters: RequestParams
    var headers: [String: String]? { nil }
    var dataType: ResponseDataType { .JSON }
}
